declare global {
    namespace JSX {
        interface Element {
            type: string
            props: any
            children: JSXChildren
        }

        interface IntrinsicElements {
            [elemName: string]: any
        }
    }

    interface Element {
        __props?: any
        __key?: string
    }

    interface Node {
        __type?: string        
    }
}

type JSXInputNode = JSX.Element | string | number | boolean | null | undefined;
type JSXInput = JSXInputNode | JSXInputNode[];

type JSXNode = JSX.Element | string;
type JSXChildren = JSXNode[];

const TEXT = "#text";
const noProps = {};

/**
 * This is the JSX factory function
 */
export function make(type: string, props: any, ...children: JSXInput[]): JSX.Element {
    let i = 0;

    // Here we fold deeply nested arrays and remove useless input, which
    // leads to a much simpler model for the other functions to work with:

    while (i < children.length) {
        const child = children[i] as any;

        if (Array.isArray(child)) {
            children.splice(i, 1, ...child);
        } else {
            if (typeof child === "number") {
                children[i] = "" + child;
            } else if (child === true || child === false || child === "" || child == null) {
                children.splice(i, 1);

                continue;
            }

            i += 1;
        }
    }

    return { type, props: props || noProps, children: children as JSXChildren };
}

function createElement(vnode: JSX.Element): Element {
    let el = document.createElement(vnode.type);

    el.__type = vnode.type;
    el.__key = vnode.props.key;

    return el;
}

function createTextNode(text: string): Text {
    let node = document.createTextNode(text);

    node.__type = TEXT;

    return node;
}

function createNode(vnode: JSXNode): Node {
    return typeof vnode === "string"
        ? createTextNode(vnode)
        : createElement(vnode);
}

function nodeType(vnode: JSXNode): string {
    return typeof vnode === "string"
        ? TEXT
        : vnode.type;
}

/**
 * Update the state of a given target Node to the state of a JSX Node.
 * 
 * If no target is given, a new target Node will be created and returned.
 * 
 * If the given target Node is of the wrong type (or is an unmanaged DOM Node) the
 * target will be replaced, and the created replacement Node will be returned.
 */
export function update(vnode: JSXNode, target?: Node): Node {
    if (target == null) {
        target = createNode(vnode);
    } else if (target.__type !== nodeType(vnode)) {
        if (target.parentNode) {
            const replacement = createNode(vnode);

            target.parentNode.replaceChild(replacement, target);

            target = replacement;
        }
    }

    if (typeof vnode === "object") {
        applyProps(vnode, target as Element);

        updateChildren(target as Element, vnode.children);
    }

    return target;
}

/**
 * Apply the properties of a JSX Element to a given target DOM Element.
 */
function applyProps(vnode: JSX.Element, target: Element): void {
    let oldProps = target.__props || noProps;
    let newProps = vnode.props;
    let allProps = target.__props
        ? Object.assign({}, oldProps, newProps) // TODO can we avoid creating an unused object to get the union of keys?
        : newProps;

    for (let name in allProps) {
        let value = newProps[name];

        if (value !== oldProps[name]) {
            let type = typeof value;

            if (value == null) {
                (target as any)[name] = null;

                target.removeAttribute(name);
            } else if (name in target) {
                (target as any)[name] = value;
            } else if (type === "string" || type === "number") {
                target.setAttribute(name, value);
            } else {
                throw Error(`unsupported property: ${name}`);
            }
        }
    }

    target.__props = newProps;
}

function max(a: number, b: number) {
    return a > b ? a : b;
}

function isManaged(node: Node) {
    return node.__type != null;
}

function remove(target: Node) {
    // TODO deferred removal
    target.parentNode!.removeChild(target);
}

export function updateChildren(parent: Element, children: JSXChildren) {
    let targets = [...parent.childNodes];
    let length = max(targets.length, children.length);

    let targetIndex = 0;
    let childIndex = 0;

    let keyTarget: { [key: string]: Element } = {};

    for (let target of targets) {
        let key = (target as Element).__key;

        if (key) {
            keyTarget[key] = target as Element;
        }
    }

    while (targetIndex < length || childIndex < length) {
        let target = targets[targetIndex];
        let child = children[childIndex];

        if (target) {
            if (isManaged(target)) {
                let key = (target as Element).__key;

                if (key && keyTarget[key]) {
                    // this target is a keyed element - we ignore it for now, and skip
                    // to the next target.

                    targetIndex++;

                    // note that, if this target's key matches that of an existing
                    // child, we'll deal with it below - and if it doesn't, it will
                    // get collected as garbage at the end of the loop. 
                } else if (typeof child === "object") {
                    let key = child.props.key;

                    if (keyTarget[key]) {
                        // a keyed target matches the child's key, so we update
                        // the keyed target to the child state:

                        update(child, keyTarget[key]);

                        // we also need to check if the keyed target is positioned
                        // correctly, and reposition it if not:

                        // TODO make this conditional on the current position
                        parent.insertBefore(keyTarget[key], target);

                        // we move the consumed key target from the map, since
                        // any keyed targets remaining in the map will get collected
                        // as garbage after we exit the loop.

                        delete keyTarget[key];

                        // note that we didn't consume the target at the current
                        // target index, because the child was mapped to a 
                        // keyed target, so we don't increase the targeIndex here.
                    } else {
                        // we're dealing with a non-keyed target and a non-keyed
                        // child, so we'll update the target to the child state:

                        update(child, target);

                        // TODO reposition

                        targetIndex++;
                    }

                    // we're done with this child - move to the next one:

                    childIndex++;
                } else if (typeof child === "string") {
                    // we have a managed target without a key, and the child is a
                    // string, so we'll update the target, turning it into a text node:

                    update(child, target);

                    // we've processed the child and the target, so move to the next pair:

                    targetIndex++;
                    childIndex++;
                } else {
                    // there is no child - we've gone beyond the list of
                    // available children, so this target just gets removed,
                    // and we move on to the next target:

                    remove(target);

                    targetIndex++;
                }
            } else {
                // this target isn't managed - ignore it and continue:

                targetIndex++;
            }
        } else {
            // there is no target - we've gone beyond the list of
            // available targets, so this child just needs to be
            // created and positioned before the next target:

            if (child) {
                target = update(child);

                parent.appendChild(target);
            }

            targetIndex++;
            childIndex++;
        }
    }

    // clean up any remaning keyed targets no longer in use:

    for (let key in keyTarget) {
        remove(keyTarget[key]);
    }
}
