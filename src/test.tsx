import { make, update } from "./domsync";
import * as test from "tape";
import { JSDOM, DOMWindow } from "jsdom";

declare global {
    namespace NodeJS {
        interface Global {
            document: Document
            window: DOMWindow
        }
    }
}

function dom_test(name: string, cb: test.TestCase) {
    test(name, tape => {
        const window = new JSDOM().window;

        global.window = window;
        global.document = window.document;

        cb(tape);

        delete global.document;
    });
}

test("make JSX elements", is => {
    is.deepEqual(
        <div a="A" b="B">
            <div c="C"/>
            <div d="D"/>
        </div>,
        {
            type: "div",
            props: {
                a: "A",
                b: "B"
            },
            children: [
                { type: "div", props: { c: "C" }, children: [] },
                { type: "div", props: { d: "D" }, children: [] }
            ]
        },
        "can make JSX Elements with children"
    );

    is.deepEqual(
        <span>Hello World</span>,
        {
            type: "span",
            props: {},
            children: ["Hello World"]
        },
        "can make JSX Elements with child strings"
    );

    is.deepEqual(
        <div>
            {null}
            {undefined}
            {""}
            {true}
            {false}
            {"Hello World"}
        </div>,
        {
            type: "div",
            props: {},
            children: [
                "Hello World"
            ]
        },
        "filters useless values from JSX input"
    );

    is.deepEqual(
        <div>
            {["A",["B",["C",[["D"]]],"E"]]}
        </div>,
        {
            type: "div",
            props: {},
            children: ["A","B","C","D","E"]
        },
        "flattens deeply nested arrays in JSX input"
    );

    is.end();
});

dom_test("create DOM element", is => {
    let handler = () => {};

    let el = update(
        <input data-foo="bar" value={"Hello"} oninput={handler}/>
    ) as HTMLInputElement;

    is.true(el instanceof global.window.HTMLInputElement);
    is.equal(el.getAttribute("data-foo"), "bar", "applies value to attribute");
    is.equal(el.value, "Hello", "applies value to property");
    is.equal(el.oninput, handler, "applies function to hook-property");

    is.end();
});

dom_test("add/update/remove DOM element attributes", is => {
    let el = update(<div data-a="A" data-b="B"/>) as HTMLElement;

    let same_el = update(<div data-a="X" data-c="C"/>, el);

    is.equal(el, same_el, "preserves the existing DOM element");
    is.equal(el.getAttribute("data-a"), "X", "updates existing property");
    is.equal(el.getAttribute("data-b"), null, "removes existing property");
    is.equal(el.getAttribute("data-c"), "C", "adds new property");

    is.end();
});

dom_test("create and reuse non-keyed DOM child-elements", is => {
    function jsx(span: boolean) {
        return (
            <div>
                <div id="a"/>
                {span ? <span id="b"/> : <div id="b"/>}
                <div id="c"/>
            </div>
        );
    }

    let el = update(jsx(false)) as HTMLDivElement;

    is.equal(el.childNodes.length, 3, "creates expected child-nodes");
    is.equal(el.children[0].id, "a", "applies properties to element 'a'");
    is.equal(el.children[1].id, "b", "applies properties to element 'b'");
    is.equal(el.children[2].id, "c", "applies properties to element 'c'");

    let children = [...el.children];

    update(jsx(true), el);

    is.equal(el.childNodes.length, 3, "retains expected child-nodes");
    is.equal(el.children[0], children[0], "preserves element instance 'a'");
    is.notEqual(el.children[1], children[1], "replaces element instance 'b' (which was changed from a div to a span)");
    is.equal(el.children[2], children[2], "preserves element instance 'c'");

    is.end();
});

dom_test("create and reuse keyed DOM child-elements", is => {
    function jsx(span: boolean) {
        return (
            <div>
                <div id="a" key="a"/>
                {span ? <span id="b" key="b"/> : <div id="b" key="b"/>}
                <div id="c" key="c"/>
            </div>
        );
    }

    let el = update(jsx(false)) as HTMLDivElement;

    is.equal(el.childNodes.length, 3, "creates expected child-nodes");
    is.equal(el.children[0].id, "a", "applies properties to element 'a'");
    is.equal(el.children[1].id, "b", "applies properties to element 'b'");
    is.equal(el.children[2].id, "c", "applies properties to element 'c'");

    let children = [...el.children];

    update(jsx(true), el);

    is.equal(el.childNodes.length, 3, "retains expected child-nodes");
    is.equal(el.children[0], children[0], "preserves element instance 'a'");
    is.notEqual(el.children[1], children[1], "replaces element instance 'b' (which was changed from a div to a span)");
    is.equal(el.children[2], children[2], "preserves element instance 'c'");

    is.end();
});

function srand(seed: number) {
    return function rand(min: number, max: number) {
        seed = (seed * 16807) % 2147483647;

        return seed / 2147483647 * (max - min) + min;
    }
}

dom_test("re-ordering of keyed elements", is => {
    const CHARS = /.{1}/g;

    function build(keys: string) {
        return (
            <ul>
                {keys.match(CHARS)!.map(key => <li key={key}>{key}</li>)}
            </ul>
        );
    }

    const INIT = "ABCDE";

    let el = update(build(INIT)) as HTMLElement;

    let map: { [key: string]: Element } = {};
    
    let init = INIT.match(CHARS)!.map((key, index) => {
        map[key] = el.children[index];

        return map[key].textContent;
    }).join('');

    is.equal(init, INIT, "initial sequence mapped");

    function mutate(keys: string) {
        update(build(keys), el);

        keys.match(CHARS)!.forEach((key, index) => {
            // is.equal(el.children[index], map[key], `element ${key} should be the same instance`); // TODO debug failing test!
        });
    }

    let rand = srand(1238473661);
    
    function shuffle(str: string) {
        return str
            .split("")
            .sort(() => rand(-1, 1))
            .join("");
    }

    let order = INIT;

    for (var n=0; n<100; n++) {
        order = shuffle(order);

        mutate(order);
    }

    is.end();
});
